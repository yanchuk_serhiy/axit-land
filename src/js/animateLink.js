$(document).ready(function(){
    $(".desktop-nav, .mobile-nav").on("click","a", function (event) {
        event.preventDefault();
        var id  = $(this).attr('href'),
        top = $(id).offset().top;
        $('body,html').animate({scrollTop: top - 50}, 1500);
    });
});


