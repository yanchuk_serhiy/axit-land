function validate(form) {
    var form = $(form);
    var inputs = form.find('input');

    var valid = true;


    if (inputs.length > 0){
        for (var i = 0; i < inputs.length; i++){
            var validTMP = validateInput($(inputs[i]));

            if(valid){
                valid = validTMP;
            }
        }
    }

    return valid;
}

function validateInput(el) {
    var valid = true;
    if (el.val() === ''){
        el.next('span').addClass('focus');
        el.addClass('incorect');
        el.attr('placeholder', 'required...');
        valid = false;
    }else{
        var emailCorrect = true;

        if(el.attr('name') === 'email'){
            emailCorrect = validateEmail(el.val());
        }


        if (emailCorrect){
            el.removeClass('incorect');
            el.attr('placeholder', el.attr('data-placeholder'));
            valid = true;
        }else{
            el.next('span').addClass('focus');
            el.val('');
            el.addClass('incorect');
            el.attr('placeholder', 'incorrect email');
            valid = false;
        }

        if(el.attr('name') === 'password'|| el.attr('name') === 'name'){
            valid = validateAlphaNumerical(el.val());

            if (!valid){
                el.next('span').addClass('focus');
                el.attr('placeholder', 'Enter correct data...');
                el.val('');
                el.addClass('incorect');
            } else{
                if(el.val().length < 6 ){
                    el.next('span').addClass('focus');
                    el.attr('placeholder', 'min 6...');
                    el.val('');
                    el.addClass('incorect');
                } else{
                    el.removeClass('incorect');
                    el.attr('placeholder', el.attr('data-placeholder'));
                }
            }

        }

    }
    
    return valid;
}

function validateAlphaNumerical(value){
    if( /[^a-zА-яA-Z0-9і]/.test( value ) ) {
        return false;
    }
    return true;
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

$('.send_messege').on('click', function () {
    var textarea =  $('#message').val();

    if(textarea == ''){
        $('#message').next('span').addClass('focus');
        $('#message').addClass('incorect');
        $('#message').attr('placeholder', 'required...');
    } else{
        $('#message').removeClass('incorect');
    }
});

$('#message').on('focus', function () {
   $(this).next('span').addClass('focus');
});