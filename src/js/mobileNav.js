new Vue ({
    el: '#mobile-nav',
    data: {
        isActive: false
    },
    methods: {
        openNav: function () {
            this.isActive = true;
        },

        closeNav: function () {
            this.isActive = false;
        }
    }
});