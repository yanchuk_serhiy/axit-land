import './plugins/owl/scss/owl.carousel.scss'
import './plugins/owl/scss/owl.theme.default.scss'

import './scss/base.scss'
import './scss/header.scss'
import './scss/home.scss'
import './scss/social_media.scss'
import './scss/tabs-section.scss'
import './scss/feature_2.scss'
import './scss/feature_3.scss'
import './scss/our_process.scss'

import './scss/pricing.scss'

import './scss/testimonials.scss'
import './scss/download_section.scss'

import './scss/contact_us.scss'

import './scss/mobile-nav.scss'
import './scss/footer.scss'

import './plugins/owl/js/owl.carousel'
import './plugins/owl/js/owl.navigation'
import './plugins/owl/js/owl.autoplay'
import './js/base'

import './js/mobileNav'







